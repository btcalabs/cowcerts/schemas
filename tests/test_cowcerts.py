"""Tests blockcerts examples against our schemas."""

# Libraries
import sys
from unittest import TestCase

# Relative imports
from .utils import validate
sys.path.append("blockcerts")
from cert_schema import BlockcertValidationError


class TestCowcerts(TestCase):

    def test_recipient_claim(self):
        self.assertTrue(
            validate(
                "cowcerts/examples/0.1/recipientClaim.json",
                "cowcerts/schema/0.1/recipientClaimSchema.json"
            )
        )

    def test_ministry_claim(self):
        self.assertTrue(
            validate(
                "cowcerts/examples/0.1/ministryClaim.json",
                "cowcerts/schema/0.1/ministryClaimSchema.json"
            )
        )

    def test_sed_claim(self):
        self.assertTrue(
            validate(
                "cowcerts/examples/0.1/EDSClaim.json",
                "cowcerts/schema/0.1/EDSClaimSchema.json"
            )
        )

    def test_full_valid(self):
        self.assertTrue(
            validate(
                "cowcerts/examples/0.1/full_valid_assertion.json",
                "cowcerts/schema/0.1/assertionSchema.json"
            )
        )

    def test_missing_fields(self):
        with self.assertRaises(BlockcertValidationError):
            raise BlockcertValidationError
