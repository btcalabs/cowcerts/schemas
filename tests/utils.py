import logging
import json
import jsonschema
import sys


sys.path.append("blockcerts")

# Relative imports
from cert_schema import BlockcertValidationError


def validate_json(certificate_json, schema_json):
    """
    If no exception is raised, the instance is valid. Raises BlockcertValidationError is validation fails.
    :param certificate_json:
    :param schema_json:
    :return:
    """
    try:
        jsonschema.validate(certificate_json, schema_json,
                            format_checker=jsonschema.draft4_format_checker)
        return True
    except jsonschema.exceptions.ValidationError as ve:
        logging.error(ve, exc_info=True)
        raise BlockcertValidationError(ve)


def validate(data_file, schema_file):
    with open(data_file) as data_f, open(schema_file) as schema_f:
        data = json.load(data_f)
        schema = json.load(schema_f)
        return validate_json(data, schema)
