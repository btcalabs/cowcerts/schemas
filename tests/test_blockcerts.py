"""Tests blockcerts examples against our schemas."""

# Libraries
import sys
from unittest import TestCase

sys.path.append("blockcerts")

# Relative imports
from cert_schema import schema_validator, BlockcertValidationError


class TestBlockcerts(TestCase):
    pass
#    def test_incompatibility(self):
#        """Check that a Blockcerts assertion misses some fields."""
#        with self.assertRaisesRegex(BlockcertValidationError, "personProfile"):
#            schema_validator.validate(
#                "examples/blockcerts.json",
#                "dta_schemas/schema/0.1/assertion.json")
