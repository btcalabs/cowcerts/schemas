# """Tests DTA examples against our schemas."""
# import pytest
#
# from dta_schemas.utils import *
# from jsonschema.validators import validator_for
# from jsonschema import ValidationError
# import jsonschema
#
# class TestAssertion:
#     @pytest.mark.parametrize(
#             "version,schema",
#             tuple(get_schemas("dta", "assertion").items()))
#     def test_schema(self, version, schema):
#         """Tests the assertion schemas are valid."""
#         validator = validator_for(schema)
#         validator.check_schema(schema)
#
#     @pytest.mark.run(after="test_schema")
#     @pytest.mark.parametrize(
#             "version, example",
#             get_all_example_files(False, "dta", "assertion"))
#     def test_invalid(self, version, example):
#         """Tests invalid examples do raise validation errors."""
#         schema = get_schema("dta", "assertion", version)
#         with open(example, "r") as f:
#             with pytest.raises(ValidationError):
#                 jsonschema.validate(json.load(f), schema)
#
#     @pytest.mark.run(after="test_schema")
#     @pytest.mark.parametrize(
#             "version, example",
#             get_all_example_files(True, "dta", "assertion"))
#     def test_valid(self, version, example):
#         """Check that sample DTA assertions are valid."""
#         schema = get_schema("dta", "assertion", version)
#         with open(example, "r") as f:
#             jsonschema.validate(json.load(f), schema)
#
#     @pytest.mark.run(after="test_schema")
#     @pytest.mark.parametrize(
#             "version, example",
#             get_all_example_files(True, "dta", "assertion"))
#     def test_compatibility(self, version, example):
#         """Check that sample DTA assertions are valid with Blockcerts."""
#         schema = get_schema("blockcerts", "assertion")
#         with open(example, "r") as f:
#             jsonschema.validate(json.load(f), schema)
