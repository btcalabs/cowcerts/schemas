command ValidateSchema !pipenv run jsonschema %:p
command Test !make test
command Run !pipenv run python -m dta_schemas
command Beautify %!python -m json.tool
" command! -nargs=1 ValidateInstance !pipenv run jsonschema expand('%') <f-args>
