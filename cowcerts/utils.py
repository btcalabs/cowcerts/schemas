import glob
import json
import logging
import os
import re
from collections import OrderedDict
from typing import Sequence
import cert_schema

# Constants
# # DTA
DTA = {}
DTA["package"] = os.path.realpath(os.path.dirname(__file__))
DTA["examples"] = os.path.realpath(os.path.join(DTA["package"], "..", "examples"))
if not os.path.isdir(DTA["examples"]):
    DTA["examples"] = None
DTA["schema"] = {}
DTA["schema"]["path"] = os.path.join(DTA["package"], "schema")
DTA["schema"]["versions"] = OrderedDict(
    (v, {}) for v in
    sorted(next(os.walk(DTA["schema"]["path"]))[1])
)
for version, details in DTA["schema"]["versions"].items():
    details["assertionFile"] = os.path.join(DTA["schema"]["path"], version,
            "assertion.json")
    with open(details["assertionFile"], "r") as f:
        details["assertion"] = json.load(f)

# # Blockcerts
BLOCKCERTS = {}
BLOCKCERTS["package"] = os.path.realpath(os.path.dirname(cert_schema.__file__))
BLOCKCERTS["examples"] = os.path.realpath(
    os.path.join(BLOCKCERTS["package"], "..", "examples"))
if not os.path.isdir(BLOCKCERTS["examples"]):
    BLOCKCERTS["examples"] = None
BLOCKCERTS["schema"] = {}
BLOCKCERTS["schema"]["path"] = BLOCKCERTS["package"]
BLOCKCERTS["schema"]["versions"] = OrderedDict(
    (v, {}) for v in
    sorted(next(os.walk(BLOCKCERTS["schema"]["path"]))[1])
    if re.compile(r"^[2-9]\.\d+$").match(v)
)
for version, details in BLOCKCERTS["schema"]["versions"].items():
    details["assertionFile"] = os.path.join(BLOCKCERTS["schema"]["path"], version,
            "schema.json")
    with open(details["assertionFile"], "r") as f:
       details["assertion"] = json.load(f)
SCHEMAS = {"dta" : DTA, "blockcerts": BLOCKCERTS}

def get_versions(schema="dta") -> Sequence[str]:
    """Returns the specified schema available versions."""
    return tuple(SCHEMAS[schema]["schema"]["versions"].keys())

def get_schema(schema="dta", obj="assertion", version=None) -> dict:
    """Returns the JSON schema object for the schema and version specified.

    If no version specified, returns the latest available.
    """
    version = get_versions(schema)[-1] if version is None else version
    return SCHEMAS[schema]["schema"]["versions"][version][obj]

def get_schemas(schema="dta", obj="assertion"):
    """Returns all schema versions for the given object, indexed by key"""
    schemas = {}
    for version in get_versions(schema):
        schemas[version] = get_schema(schema, obj, version)
    return schemas

def get_all_example_files(valid=True, schema="dta", obj="assertion"):
    """Gets all example files available."""
    example_files = []
    for version in get_versions(schema):
        example_files += [
            (version, e) for e in get_example_files(valid,
            schema, obj, version)
        ]
    return example_files

def get_example_files(valid=True, schema="dta", obj="assertion", version=None):
    """Returns JSON examples of the given schema object."""
    # Defaults
    version = get_versions(schema)[-1] if version is None else version

    # File name glob
    validity = "valid" if valid else "invalid"
    filename_glob = "_".join((validity, schema, obj, version)) + "*.json"
    path_glob = os.path.join(SCHEMAS[schema]["examples"], filename_glob)

    # Look for files
    return glob.glob(path_glob)

def validate_schema_instance(instance, schema):
    """Validates the schema instance using the given schema."""
    return jsonschema.validate(instance, schema)
