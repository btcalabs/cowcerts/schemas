"""DTA Schemas entry point."""
import json
import os
import sys

# Libraries
from .utils import *

# Actions
def info():
    """Prints information about the current schemas."""
    # Remove schemas, leave just files
    import copy
    reduced_schemas = copy.deepcopy(SCHEMAS)
    for package, schema in reduced_schemas.items():
        for version, details in schema["schema"]["versions"].items():
            keys_to_delete = []
            for key, value in details.items():
                if not key.endswith("File"):
                    keys_to_delete.append(key)
            for key in keys_to_delete:
                del details[key]

    print(json.dumps(reduced_schemas, indent=4))

if __name__ == "__main__":
    info()
