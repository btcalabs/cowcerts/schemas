{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "Endorsement schema",
  "description": "Defines an endorsement that contains extra information about the assertion containing it.",
  "type": "object",
  "definitions": {
    "JsonLdType": {
      "description": "A type or an array of types defined in a referenced JSON-LD context.",
      "oneOf": [
        {
          "type": "string"
        },
        {
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      ]
    }
  },
  "properties": {
    "id": {
      "type": "string",
      "format": "iri"
    },
    "type": {
      "type": "string"
    },
    "claim": {
      "type": "object"
    },
    "issuer": {
      "$ref": "https://w3id.org/blockcerts/schema/2.1/issuerSchema.json"
    },
    "issuedOn": {
      "description": "Open Badges must express timestamps as strings compatible with ISO 8601 guidelines, including the time and a time zone indicator. It is recommended to publish all timestamps in UTC. Previous versions of Open Badges allowed Unix timestamps as integers. Open Badges v2.0 requires string ISO 8601 values with time zone indicators. For example, 2016-12-31T23:59:59+00:00 is a valid ISO 8601 timestamp. It contains the year, month, day, T separator, hour number 0-23, minute, optional seconds and decimal microsecond, and a time zone indicator (+/- an offset from UTC or the Z designator for UTC).",
      "type": "string"
    },
    "verification": {
      "description": "From https://w3id.org/openbadges#VerificationObject, with extensions for Blockcerts verification",
      "type": "object",
      "properties": {
        "type": {
          "$ref": "#/definitions/JsonLdType",
          "description": "Defined by `type` property of https://w3id.org/openbadges#VerificationObject. Blockcerts extension: this should contain the entry `MerkleProofVerification2017`"
        },
        "publicKey": {
          "type": "string",
          "anyOf": [
            {
              "type": "string",
              "pattern": "^ecdsa-koblitz-pubkey:",
              "description": "Issuer public key or blockchain address with `<scheme>:` prefix. For Bitcoin transactions, this would be the issuer public address prefixed with `ecdsa-koblitz-pubkey:`. Example: `ecdsa-koblitz-pubkey:14RZvYazz9H2DC2skBfpPVxax54g4yabxe`"
            },
            {
              "type": "string"
            }
          ],
          "description": "Blockcerts extension: the expected blockchain address for the signer of the transaction containing the merkle proof. In Blockcerts `publicKey`s are typically represented with a `<scheme>:` prefix. For Bitcoin transactions, this would be the issuer public Bitcoin address prefixed with `ecdsa-koblitz-pubkey:`; e.g. `ecdsa-koblitz-pubkey:14RZvYazz9H2DC2skBfpPVxax54g4yabxe`"
        }
      },
      "required": [
        "type"
      ]
    },
    "signature": {
      "$ref": "https://w3id.org/blockcerts/schema/2.1/merkleProof2017Schema.json"
    },
    "signatureLines": {
      "type": "array",
      "items": {
        "$ref": "https://w3id.org/blockcerts/schema/2.1/signatureLineSchema.json"
      }
    }
  },
  "required": [
    "id",
    "type",
    "claim",
    "issuer",
    "issuedOn"
  ]
}
