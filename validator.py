import json
import sys

sys.path.append("blockcerts")

from cert_schema import schema_validator

# Read arguments
if not len(sys.argv) == 3:
    print("Usage:")
    print("python validator path_to_cert path_to_schema")
    print()
    print("Example:")
    print("python validator.py blockcerts/examples/2.1/sample_valid.json blockcerts/cert_schema/2.1/schema.json")
    sys.exit(1)

print(schema_validator.validate(sys.argv[1],sys.argv[2]))
