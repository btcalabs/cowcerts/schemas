# Cowcerts Schemas checker
The _Python_ package included in the repository helps automatically testing the
developed schemas against the _Blockcerts_ standards (its schemas) and
therefore implicitly agains the _Open Badges_ standards.

# Usage
We use ``pipenv`` to manage dependencies and environments. Install it.

```bash
pip install pipenv
```

Then, run the tests

```bash
make tests
```
