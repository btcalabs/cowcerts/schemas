import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

with open('requirements.txt') as f:
    install_reqs = f.readlines()
    requirements = [str(ir) for ir in install_reqs]

with open(os.path.join(here, 'README_PYTHON.md')) as fp:
    long_description = fp.read()

setup(
    name='dta_schemas',
    version='0.0.1',
    description='DTA Project schemas based on Blockerts and Open Badges',
    author='BTCALabs.tech',
    url='https://gitlab.com/cowcerts/schemas',
    author_email='labs+cowcerts@btcassessors.com',
    long_description=long_description,
    packages=find_packages(),
    install_requires=requirements
)
