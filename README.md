# Schemas

Schemas to define the entities in the _workflow_ of granting digital academic
degree credentials.

## First, init the submodules

After cloning, please init the submodules and pull them:

```bash
git submodule update --init --recursive
```

## TL;DR
If you just want to check the schemas are compatible with the current
standards, please check the [_Python_'s package README](README_PYTHON.md)

## Purpose

The purpose of these schemas is to create digital academic degree credentials 
following the [Open Badges](https://openbadges.org) open standard along with 
the [Blockcerts](https://www.blockcerts.org) extensions to enable an easy and 
_trust-minimized_ verification using _blockchain_ technologies.

Also, those schemas MUST allow to store the current information available in
a current academic degree so the current system of official academic degrees
granting can stay the same but having digital academic degrees or _assertions_
as a complement.

These schemas will allow to comply with the standards and the current
analog academic degree credential granting processes defined in Andorra.

## Schema definitions languages
In order to be able to comply with the specified open standards, we'll use the
following definitions languages to extend those schemas.

### [JSON Schema](https://json-schema.org)
Allows to define the structure of a JSON object for validation purposes.

It's used in [Blockcerts](https://www.blockcerts.org) to validate their 
JSON-LD structures.

### [JSON Linked Data](https://json-ld.org)
Allows to define the meaning of a JSON object fields for documentation and
definition purposes to allow data interchangeability (to be used in 
[RDF](https://www.w3.org/RDF/)). **Its purpose IS NOT to validate JSON
objects.**

It's used in [Open Badges](https://openbadges.org) to meaningfully express each
academic credential field (and expanded by
[Blockcerts](https://www.blockcerts.org))

## Standard schemas specifications
In order to automatically validate compliance of our schemas with the open
standards defined and to allow having all the information in the same
repository for archival purposes, a copy of them will be available as 
[Git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) in the
current repository.


## Open Badges
Provides a JSON-LD compliant specification in HTML.

- **Local copy**
    - **Submodule folder**: ``./openbadges``
    - **JSON-LD context**: ``./openbadges/v2/context.json``
    - **JSON-LD specifications**: ``./openbadges`` __\*__
- **Published copy**
    - **JSON-LD context**: https://w3id.org/openbadges/v2 __\*\*__
    - **JSON-LD specifications**: https://w3id.org/openbadges# __\*\*\*__

> __\*__: Must be built first with [Jekyll](https://jekyllrb.com). 
>
> 1. Go to the `openbadges` submodule folder
> 2. Install dependencies with `npm i` (*weird as there are no deps!*)
> 3. Install [Jekyll](https://jekyllrb.com) (and therefore `bundle` too)
>    Make sure `jekyll` is executable
> 4. Run `npm run serve` (or just look at `package.json` and do the same
>    without `npm` 🙄: `jekyll serve`)
> 5. Open your browser at the last command output given URL
>
> **WARNING**: At the time of writing this document (20190124), the command at
> step 4 could not complete due to a generic error.

> __\*__: Redirects to https://openbadgespec.org/v2/context.json 

> __\*\*__: Redirects to https://openbadgespec.org

## Blockcerts
Provides a JSON-LD compliant specification extension using _Markdown_ files
for documentation and _JSON Schemas_ for validation.

- **Local copy**:
    - **Submodule folder**: ``./blockcerts``
    - **JSON-LD context**: ``./blockcerts/cert_schema/2.1/context.json``
    - **JSON-LD specifications**: ``./blockcerts/cert_schema/docs/schema-2.1.md``
    - **JSON Schemas**: ``./blockcerts/cert_schema/2.1``
- **Published copy**:
    - **JSON-LD context**: https://w3id.org/blockcerts/v2.1 __\*__
    - **JSON-LD specifications**: https://w3id.org/blockcerts# __\*\*__
    - **JSON schemas**: https://github.com/blockchain-certificates/cert-schema/tree/master/cert_schema/2.1

> __\*__: Redirects to https://www.blockcerts.org/schema/2.1/context.json

> __\*\*__: Redirects to https://www.blockcerts.org but the proper link would
> be its [GitHub repo](https://github.com/blockchain-certificates/cert-schema)
> [markdown document](https://github.com/blockchain-certificates/cert-schema/blob/master/docs/schema-2.1.md)
> where the schemas are defined.
