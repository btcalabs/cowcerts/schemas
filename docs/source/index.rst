==================
Cowcerts standards
==================

The **cowcerts standards** provide a specification for JSON-based data models
to issue digitally verifiable credentials like educational certificates.

To do so, we leverage the work of:

- JSON_ as the open and widely-known data interchange format
- `JSON Linked Data`_ to define the meaning of JSON fields
- `JSON Schema`_ to ensure JSON documents fulfill the specification structure

...and other standards depending on the use case.

.. _JSON: https://tools.ietf.org/html/rfc7159
.. _JSON Linked Data: https://json-ld.org
.. _JSON Schema: https://json-schema.org

.. note::
   We are now focused on **educational digital certificates** which contain the
   same information as the current analogic academic certificates, but providing
   all the advantages of digital technologies, including *digital signatures* and
   cutting edge *blockchain technologies* too.


*****************
List of standards
*****************

Below you will find the specifications for the standards we're developing:

.. toctree::
   :maxdepth: 2
   :glob:

   self
   edu/*


Contributions
=============

Please, feel free to contact us with your ideas and feedback to the following
 email address:

:email:`Cowcerts Feedback <cowcerts+feedback@btcassessors.com>`


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
