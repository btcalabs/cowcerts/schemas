==========
Blockcerts
==========

The Blockcerts_ standard provides extension to the `Open Badges`_ standard to
store the verification information of the certificate using *blockchain
technologies* to enforce the integrity and allow a distributed timestamping of
the issued certificates.

.. _Blockcerts: https://blockcerts.org
.. _Open Badges: https://openbadgespec.org

**********
Data model
**********

The data model is the same as the `Open Badges`_ one, adding the required
data so that certificates validation can be stored in a *blockchain*.

.. note::

   Details about the data model extensions are provided in the following
   document:

   https://github.com/blockchain-certificates/cert-schema/blob/master/docs/schemas-2.1.md

SignatureLine
=============
The only entity extended by Blockcerts_ which has no relation at all with
storing and validating the certificates using *blockchain technologies* is
the ``SignatureLine``. A ``SignatureLine`` allows to place a handwritten
signature picture in a certificate to improve its visualization.

.. note::
   Blockcerts ``SignatureLine`` specification:

   https://github.com/blockchain-certificates/cert-schema/blob/master/docs/signatureLineExtension_schema.md


**********
Validation
**********

In order to enforce the security of the certificate upon validating it,
Blockcerts uses *blockchain technologies* as a way to store information to
validate the certificate, including in it a hash and a digital signature.

.. tip::

   Check this document for more information about the verification process:

   https://github.com/blockchain-certificates/cert-verifier-js/blob/master/docs/verification-process.md


