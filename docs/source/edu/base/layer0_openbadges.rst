===========
Open Badges
===========

`Open Badges`_ provides the JSON data model to represent achievements issued
by some entity to a given recipient.

**********
Data model
**********

The core entities they define are:

Profile
=======

Data model for an entity, either a recipient of an achievement, or its issuer.
Provides details about them (name, description, image) and how to contact
them (email, phone, URL).


.. hint::
   For instance, a university that issues certificates like the `M.I.T.` or the
   recipient who completes a course.

.. _M.I.T.: https://mit.edu

.. note::
   In practice, **Profile** is a data model just used for certificate issuers.

.. note::
   Open Badges ``Profile`` Specification:

   https://openbadgespec.org#Profile

Badge
=====

Data model to represent an achievement, issued by an entity defined by the
previous Profile_ data model. They provide information about the achievement
obtained, the knowledge obtained, what was required to obtained the
achievement and more.

.. hint::
   For instance, a Computer Science degree by the `M.I.T.`_ could be a badge.


.. note::
   It is also referred to as **BadgeClass**, but we call them **Badges** for
   short.

.. note::
   Open Badges ``Badge`` (``BadgeClass``) Specification:

   https://openbadgespec.org#BadgeClass

Assertion
=========

Data model to issue a badge to a given recipient. Includes the badge issued,
the recipient who receives the badge and data about the issuance, like data
to verify if the assertion is valid or not.

.. hint::
   For instance, a Computer Science degree by the `M.I.T.`_ issued to Silvio
   Micali could be an assertion.

.. note::
   In previous versions it was called a **Badge** but this lead to `overloaded
   meaning`__ of the badge word. Please avoid using **Badge** to refer to a
   badge issued to a recipient and refer to it as an **Assertion**. The badge
   is just an achievement without any recipient.

.. _badge-assertion: https://github.com/IMSGlobal/openbadges-specification/pull/214

__ badge-assertion_

.. note::
   Open Badges ``Assertion`` Specification:

   https://openbadgespec.org#Assertion

Endorsement
===========

Data model to provide extra information about an Assertion_, either to
provide more information about the Assertion_, comments about the existing
information or validation information. The standard is open to receive any kind
of extra information about a given assertion.

.. note::
   Open Badges ``Endorsement`` Specification:

   https://openbadgespec.org#Endorsement

**********
Validation
**********

In order to ensure that the certificates following this data model can be
verified (to avoid fake certificates with impersonation), Open Badges
provides two kinds of validations:

Hosted
======

The certificate is valid if is hosted on a trusted place.

.. hint::

   For instance, a certificate issued by an issuer whose URL is https://mit.edu
   is valid just if the certificate can be downloaded from an URL related to
   the domain ``mit.edu``

.. note::
   Open Badges ``HostedVerification`` Specification:

   https://openbadgespec.org#HostedBadge

Signed
======

The certificate contains an `RSA signature`_ using `JWS`_. Therefore you can
know that if the certificate signature is valid, the certificate was signed
and therefore issued by the specified RSA public key.

.. _RSA signature: https://en.wikipedia.org/wiki/RSA_(cryptosystem)
.. _JWS: https://tools.ietf.org/html/rfc7515

.. attention::

   The standard does not care about the real world™ identity of the holder of
   the private key. Therefore you can know whether the certificate was issued
   by a public key, but not who in the real world™ is behind the control of the
   signatures made by the private key matching that public key.

   Using a PKI_ could be a solution for that, but that's not specified in the
   standard.

.. _PKI: https://en.wikipedia.org/wiki/Public_key_infrastructure
