======================
Cowcerts for Education
======================

**Cowcerts for Education** provides a data model for digital certificates to
prove that someone has completed their education successfully.

They aim to be a digital replacement for current educational certificates
issued on paper, leveraging all digital technologies' advantages.

********
Features
********

Our goals with this standards are to provide:

- **Security**:
  Easy validation of the certificates using *digital signatures* and *blockchain
  technologies*. The identites of both the issuer and recipient are also
  easily and automatically validated.

- **Shareability**:
  The recipient of a certificate can easily share its educational
  achievements digitally using their preferred communication channels: social
  networks, email, instant messaging and much more.

- **Privacy**:
  The recipient is allowed to share just the information they want, as the
  certificate itself does not contain any personal information or any
  detail about the achievement like the grades obtained. Certificates'
  recipients can choose the amount information they share when sharing the
  certificate.


**************
Base standards
**************

**Cowcerts for Education** provides extensions to the following base standards:

- `Open Badges`_ standard
- `Blockcerts`_ standard

.. _Open Badges: https://openbadgespec.org
.. _Blockcerts: https://blockcerts.org

In order to get a basic comprehension of the standard, please read our
abstract of the base standards:

.. toctree::
   :maxdepth: 2
   :caption: Abstracts of base standards
   :glob:

   base/*

*************
Specification
*************

The specification comprehends the following documents:

.. toctree::
   :maxdepth: 3
   :glob:

   specs/*
