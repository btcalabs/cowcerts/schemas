.. TODO: Custom HTML anchors to match context.json names

======================
JSON-LD Specifications
======================

The following document provides a specification for the JSON-LD_ context names
used in CowCerts JSON certificates.

.. _JSON-LD: https://json-ld.org

All specifications here extend the Blockcerts_ context specifications (
available in a `markdown document`__)

.. _blockcerts-json-ld: https://github.com/blockchain-certificates/cert-schema/tree/master/docs

__ blockcerts-json-ld_

.. _Blockcerts: https://www.blockcerts.org

And Blockcerts_ extends the `Open Badges`_ specifications

.. _Open Badges: https://openbadgespec.org

********
Entities
********
These are the extended JSON entities we define to achieve the digital granting
of academic certificates. We also define the generic entities so that all the
document can be understand within this single specifications document.


Assertion
=========
Assertions are representations of an awarded badge, used to share information
about a badge belonging to one earner. Assertions are packaged for transmission
as JSON objects with a set of mandatory and optional properties. Fields marked
in bold letters are mandatory.

+----------------+-------------------+----------------------------------------+
| Property       | Expected type     | Description                            |
+================+===================+========================================+
| **id**         | ``IRI``           | Uniquely identifies the assertion.     |
|                |                   | Should be an ``HTTPS`` dereferenceable |
|                |                   | IRI so verifiers can link to the       |
|                |                   | assertion. The document behind the IRI |
|                |                   | should provide information about the   |
|                |                   | assertion and its validation.          |
+----------------+-------------------+----------------------------------------+
| **type**       | JSON-LD type      | JSON-LD assertion type.                |
|                |                   | The type is fixed to: ``Assertion``.   |
+----------------+-------------------+----------------------------------------+
| **recipient**  | ``IdentityObject``| The recipient of the achievement.      |
+----------------+-------------------+----------------------------------------+
| **badge**      | ``BadgeClass``    | Badge document being awarded.          |
+----------------+-------------------+----------------------------------------+
|**verification**| ``Verification    | A collection of information allowing   |
|                | Object``          | an inspector to verify this assertion. |
+----------------+-------------------+----------------------------------------+
| **issuedOn**   | ``DateTime``      | Timestamp of when the achievement was  |
|                |                   | awarded. ISO 8601 compliant.           |
+----------------+-------------------+----------------------------------------+
| image          | ``ImageObject``   | Image representing this achievement.   |
+----------------+-------------------+----------------------------------------+
| recipient\     | ``Recipient       | Blockcerts_ extension allowing         |
| Profile        | Profile``         | additional recipient details including |
|                |                   | recipient's public key to make a       |
|                |                   | strong claim of the ownership over the |
|                |                   | key.                                   |
|                |                   |                                        |
|                |                   | Name must be set if the verifier       |
|                |                   | provided by Blockcerts_ has to display |
|                |                   | it.                                    |
+----------------+-------------------+----------------------------------------+
| signature      |``MerkleProof2019``| An extension that allows an issuer to  |
|                |                   | issue an Open Badge on the blockchain  |
|                |                   | and provide proof of inclusion in a    |
|                |                   | blockchain transaction. This uses      |
|                |                   | `Merkle Proof Signature Suite 2019`__. |
+----------------+-------------------+----------------------------------------+
| displayHtml    | ``Text``          | HTML code to display the certificate   |
|                |                   | Will be used in the                    |
|                |                   | `blockcerts verifier`_ to display the  |
|                |                   | certificate visually.                  |
|                |                   | This way the certificate will always   |
|                |                   | be visualized the same way even if the |
|                |                   | certificate displayer (in this case    |
|                |                   | `blockcerts verifier`_) changes.       |
+----------------+-------------------+----------------------------------------+
| issuedAt       | ``Text``          | Place where the assertion was issued   |
|                |                   | (the city for instance)                |
+----------------+-------------------+----------------------------------------+
| official\      | ``Endorsement``   | Mandatory field if the ``BagdeClass``  |
| Validation     |                   | being awarded has an officiality       |
|                |                   | requirement. This ``Endorsement`` has  |
|                |                   | always a ``MinistryClaim`` it its      |
|                |                   | claim field.                           |
+----------------+-------------------+----------------------------------------+
| signatureLines | ``Array`` of      | List of handwritten signatures that    |
|                | ``SignatureLine`` | must appear in the certificate         |
|                |                   | visualization                          |
+----------------+-------------------+----------------------------------------+
| universal\     | ``Text``          | Universal identifier of the assertion. |
| Identifier     |                   | Allows to identify the assertion       |
|                |                   | uniquely in the universe.              |
|                |                   | Contains a ``UUID`` string.            |
+----------------+-------------------+----------------------------------------+

__ MerkleProof2019_
.. _blockcerts verifier: https://github.com/blockchain-certificates/blockcerts-verifier


IdentityObject
==============
A collection of information about the recipient of a ``IdentityObject``.

.. note::
   The Open Badges standard does not allow for a recipient to have more than one
   field (or a document) identifying it. For this reason, we use the
   government's tax ID in this identify object and all extra fields will be
   placed in an additional assertion, referencing this identity object.

   To provide optional additional privacy to the recipient, this field will be
   always hashed.

+--------------+------------------+-------------------------------------------+
| Property     | Expected type    | Description                               |
+==============+==================+===========================================+
| **identity** | ``IdentityHash`` | The hash of student email.                |
+--------------+------------------+-------------------------------------------+
| **type**     | ``Property IRI`` | Fixed ``email``, as Blockcerts specifies. |
|              |                  |                                           |
|              |                  | Extra fields will be placed in an         |
|              |                  | additional endorsement.                   |
+--------------+------------------+-------------------------------------------+
| **hashed**   | ``Boolean``      | Always ``True``.                          |
+--------------+------------------+-------------------------------------------+
| **salt**     | ``Text``         | If the recipient is hashed, this should   |
|              |                  | contain the string used to salt the hash. |
|              |                  | If this value is not provided,            |
|              |                  | it should be assumed that the hash        |
|              |                  | was not salted.                           |
|              |                  |                                           |
|              |                  | This field is **mandatory**, as opposite  |
|              |                  | to the Open Badges specification.         |
+--------------+------------------+-------------------------------------------+

IdentityHash
------------
A hash string preceded by a dollar sign (“$”) and the algorithm used to
generate the hash. The supported algorithms are MD5 and SHA-256, identified by
the strings md5 and sha256 respectively.

.. hint::
   For example:

   ``sha256$28d50415252ab6c689a54413da15b083034b66e5``

   represents the result of calculating a SHA-256 hash on the string “mayze”.

For more information, see `how to hash & salt in various languages`__.

.. _how-to-hash-salt: https://github.com/mozilla/openbadges-backpack/wiki/How-to-hash-&-salt-in-various-languages.

__ how-to-hash-salt_


BadgeClass
==========
A collection of information about a badge.

+----------------+-------------------+----------------------------------------+
| Property       | Expected type     | Description                            |
+================+===================+========================================+
| **id**         | ``Text``          | Unique IRI for the BadgeClass.         |
+----------------+-------------------+----------------------------------------+
| **type**       | JSON-LD type      | JSON-LD assertion type.                |
|                |                   | The type is fixed to: ``BadgeClass``.  |
+----------------+-------------------+----------------------------------------+
| **name**       | ``Text``          | Name of the badge defined in the       |
|                |                   | syllabus.                              |
+----------------+-------------------+----------------------------------------+
| **description**| ``Text``          | Description of the knowledge acquired  |
|                |                   | by a recipient of this badge.          |
+----------------+-------------------+----------------------------------------+
| **image**      |``ImageObject``    | Image representing the badge.          |
+----------------+-------------------+----------------------------------------+
| **criteria**   |``@id:Criteria``   | URI or embedded criteria document      |
|                |                   | describing how to earn the achievement.|
+----------------+-------------------+----------------------------------------+
| **issuer**     | ``Profile``       | IRI or document describing the         |
|                |                   | individual, entity, or organization    |
|                |                   | that issued the badge.                 |
+----------------+-------------------+----------------------------------------+
| signatureLines | Array of          | List of visual signatures to display.  |
|                | ``SignatureLine`` |                                        |
+----------------+-------------------+----------------------------------------+
| tags           | Array of ``Text`` | An array containing tags that          |
+----------------+-------------------+----------------------------------------+
| legalText      | ``Text``          | Additional information to the title    |
|                |                   | that refers to its legality            |
|                |                   | (legal text).                          |
|                |                   | describes the type of achievement.     |
+----------------+-------------------+----------------------------------------+
| official       | ``Boolean``       | ``True`` if requires an official       |
|                |                   | validation. Defaults to ``False`` if   |
|                |                   | field is not present.                  |
+----------------+-------------------+----------------------------------------+

ImageObject
===========
Metadata about images that represent ``Assertions``, ``BadgeClasses`` or
``Profile``.

These properties can typically be represented as just the id string of the
image, but using a fleshed-out document allows for including captions and
other applicable metadata. https://schema.org/ImageObject

+----------------+------------------+-----------------------------------------+
| Property       | Expected type    | Description                             |
+================+==================+=========================================+
| **type**       | JSON-LD type     | Type of the image object.               |
|                |                  | Default is ``schema:ImageObject``.      |
+----------------+------------------+-----------------------------------------+
| **id**         | ``IRI``          | Data URI of the image.                  |
+----------------+------------------+-----------------------------------------+
| caption        | ``Text``         | Caption of the image, if any.           |
+----------------+------------------+-----------------------------------------+


Criteria
========
Descriptive metadata about the achievements necessary to be recognized with an
``Assertion`` of a particular ``BadgeClass``.

+----------------+------------------+-----------------------------------------+
| Property       | Expected type    | Description                             |
+================+==================+=========================================+
| **type**       | JSON-LD type     | Type of the image object.               |
|                |                  | Fixed: ``Criteria``.                    |
+----------------+------------------+-----------------------------------------+
| **narrative**  | ``Text``         | A narrative of what is needed to earn   |
|                |                  | the badge.                              |
+----------------+------------------+-----------------------------------------+
| **id**         | ``IRI``          | The URI of a webpage that describes in a|
|                |                  | human-readable format the criteria for  |
|                |                  | the ``BadgeClass``.                     |
+----------------+------------------+-----------------------------------------+


Profile
=======

A Profile is a collection of information that describes the entity or
organization using `Open Badges`_.

+----------------+------------------+-----------------------------------------+
| Property       | Expected type    | Description                             |
+================+==================+=========================================+
| **id**         | ``IRI``          | Issuer blockchain address IRI,  defined |
|                |                  | in  `issuerSchema`_ by blockcerts.      |
+----------------+------------------+-----------------------------------------+
| **type**       | JSON-LD type     | Fixed: ``Profile``.                     |
+----------------+------------------+-----------------------------------------+
| name           | ``Text``         | The name of the entity or organization. |
+----------------+------------------+-----------------------------------------+
| url            | ``IRI``          | The homepage or social media profile of |
|                |                  | the entity, accessible via HTTP.        |
+----------------+------------------+-----------------------------------------+
| telephone      | ``Text``         | A phone number for the entity           |
|                |                  | (`E.164`_ format).                      |
+----------------+------------------+-----------------------------------------+
| description    | ``Text``         | A short description of the issuer,      |
|                |                  | public or private and year of creation  |
+----------------+------------------+-----------------------------------------+
| image          | ``ImageObject``  | An image representing the issuer.       |
+----------------+------------------+-----------------------------------------+
| email          | ``Text``         | Contact address for the individual or   |
|                |                  | organization.                           |
+----------------+------------------+-----------------------------------------+
| revocationList | ``URI``          | HTTP URI of the Badge Revocation List   |
|                |                  | used for marking revocation of signed   |
|                |                  | badges. The revocation list is          |
|                |                  | published as a JSON-LD document with    |
|                |                  | type ``RevocationList``.                |
+----------------+------------------+-----------------------------------------+

.. _E.164: https://en.wikipedia.org/wiki/E.164
.. _issuerSchema: https://www.blockcerts.org/schema/2.1/issuerSchema.json


RevocationList
==============
List of assertions revoked by the issuer of the degree.

+----------------+------------------+-----------------------------------------+
| Property       | Expected type    | Description                             |
+================+==================+=========================================+
| **id**         | ``IRI``          | The ``id`` of the RevocationList.       |
+----------------+------------------+-----------------------------------------+
| **type**       | JSON-LD type     | Fixed ``Profile``.                      |
+----------------+------------------+-----------------------------------------+
| **issuer**     | ``IRI: Profile`` | The ``id`` of the Issuer.               |
+----------------+------------------+-----------------------------------------+
| **revoked\     | ``IRI``          | A string ``id`` identification of a     |
| Assertions**   |                  | badge object that has been revoked. And |
|                |                  | a string ``revocationReason`` indicate  |
|                |                  | the reason for revocation.              |
+----------------+------------------+-----------------------------------------+


VerificationObject
==================
Defined by verification property of https://w3id.org/openbadges#Assertion,
with Blockcerts extensions for verification of badges on a blockchain.

+----------------+------------------+-----------------------------------------+
| Property       | Expected type    | Description.                            |
+================+==================+=========================================+
| **type**       | ``Array``        | Fixed: ``MerkleProofVerification2017``, |
|                |                  | ``Extension``                           |
+----------------+------------------+-----------------------------------------+
| **publicKey**  | ``Text``         | Blockcerts extension: the expected      |
|                |                  | blockchain address for the signer of    |
|                |                  | the transaction containing the merkle   |
|                |                  | proof. In Blockcerts publicKeys are     |
|                |                  | typically represented with a            |
|                |                  | ``<scheme>:`` prefix. For Bitcoin       |
|                |                  | transactions, this would be the issuer  |
|                |                  | public Bitcoin address prefixed with    |
|                |                  | ``ecdsa-koblitz-pubkey:``.              |
+----------------+------------------+-----------------------------------------+


MerkleProof2019
===============

.. _MerkleProof2019:

Extends the `Merkle Proof 2017`_ verification allowing to contain additional
information about the assertion: a set of endorsements_. They also contain
extra information to verify the certificates in other blockchains other than
the ones accepted by Blockcerts_ (for instance, the BlockValley_ ones).

.. _BlockValley: https://blockvalley.info

Some endorsements are completely optional and are signed by their issuers
independently so they cannot be included inside the assertion as when signing
it the signature would contain the endorsements, which as we said are optional
and may appear in the future.

.. note::
   The *signature* field is the only field in an assertion that is not signed
   (because it contains the signature itself) therefore is the only place where
   these endorsements fit. They may be present or not to provide extra
   information about the assertion, but without them the assertion is valid.

   This way we can package all information about the assertion, including its
   endorsements in a single portable document.

+----------------+------------------+-----------------------------------------+
| Property       | Expected type    | Description                             |
+================+==================+=========================================+
| **type**       | ``Array``        | Composed type of ``Extension`` and      |
|                |                  | ``MerkleProofVerification2019``.        |
+----------------+------------------+-----------------------------------------+
| **merkleRoot** | ``Text``         | Batch identifier (SHA256).              |
+----------------+------------------+-----------------------------------------+
| **targetHash** | ``Text``         | Current document's SHA256 hash.         |
+----------------+------------------+-----------------------------------------+
| **anchors**    | ``Array`` of     | Array containing a list of anchor       |
|                | Anchor_ objects  | objects that define how the             |
|                |                  | ``merkleRoot`` has been registered in   |
|                |                  | one or more *blockchains*.              |
+----------------+------------------+-----------------------------------------+
| **proof**      | ``Text``         | See `Merkle Proof 2017`_ for more.      |
+----------------+------------------+-----------------------------------------+
| endorsements   | Array of         | Endorsements containing additional      |
|                | ``Endorsement``  | information about the current document. |
+----------------+------------------+-----------------------------------------+

.. _endorsements: https://openbadgespec.org#Endorsement
.. _Merkle Proof 2017: https://w3c-dvcg.github.io/lds-merkleproof2017

Anchor
------
Specifies how the ``merkleRoot`` in a ``Merkle Proof 2019`` signature has been
registered on a *blockchain*.

+----------------+------------------+-----------------------------------------+
| Property       | Expected type    | Description                             |
+================+==================+=========================================+
| **type**       | JSON-LD type     | ``BTCOpReturn`` or ``ETHData`` defined  |
|                |                  | in https://chainpoint.org/ .            |
+----------------+------------------+-----------------------------------------+
| **sourceId**   | ``Text``         | Identifier, such as a transaction id,   |
|                |                  | used to locate anchored data.           |
+----------------+------------------+-----------------------------------------+
| **chain**      | ``Text``         | Chain is an optional field introduced   |
|                |                  | by Blockcerts_ to help during           |
|                |                  | verification.                           |
|                |                  |                                         |
|                |                  | Check `Merkle Proof Signature 2017`_    |
|                |                  | specifications document to see a list   |
|                |                  | of accepted chains.                     |
+----------------+------------------+-----------------------------------------+
| otherChains    | ``Array`` of     | Allows to verify the certificate in     |
|                | `Other chain`_   | other chains other than the accepted by |
|                | objects          | Blockcerts_. This way Blockcerts_ can   |
|                |                  | verify with a chain they consider valid |
|                |                  | and other verifiers may supply other    |
|                |                  | *blockchains* to verify this            |
|                |                  | on too.                                 |
+----------------+------------------+-----------------------------------------+

.. _Merkle Proof Signature 2017: https://github.com/blockchain-certificates/cert-schema/blob/master/docs/merkleProofSignatureExtension_schema.md#chain-string

Other chain
-----------

Allows to define another chain where an anchor may be placed other than the
accepted by the Blockcerts_ standard to be used by validators that both
understand Blockcerts_ and Cowcerts_ standards.

.. _Cowcerts: https://cowcerts.org

+----------------+------------------+-----------------------------------------+
| Property       | Expected type    | Description                             |
+================+==================+=========================================+
| **id**         | ``IRI``          | Chain id. Should be able to be          |
|                |                  | dereferrenced so the document behind    |
|                |                  | provides more information about the     |
|                |                  | chain and how to connect to it.         |
+----------------+------------------+-----------------------------------------+
| **name**       | ``Text``         | Commonly used name to identify the      |
|                |                  | chain.                                  |
+----------------+------------------+-----------------------------------------+
| **protocol**   | ``Text``         | Protocol the *blockchain* uses.         |
|                |                  | Valid values are ``ETH`` for            |
|                |                  | *Ethereum*.                             |
+----------------+------------------+-----------------------------------------+
| **genesis**    | ``Text``         | Hexadecimal string representing the     |
|                |                  | genesis block hash for the network.     |
+----------------+------------------+-----------------------------------------+
| consortium     | ``IRI``          | Consortium where the chain belongs      |
|                |                  | (if any).                               |
+----------------+------------------+-----------------------------------------+

.. note::

   If the ``id`` is ``urn:example:local``, the verifier of the certificate will
   connect against a local development *blockchain* so that *blockchain* checks
   can be triggered against a dummy *blockchain*.

   In the case of protocol ``ETH``, checks will be performed against a local
   *Ethereum* node using the ``web3`` JSON-RPC at default port 8545 against the
   same host where the verifier is running.

   A note will alert the user that the *blockchain* checks are performed against
   a dummy blockchain and must not be taken seriously.


Endorsement
===========

The endorsement class is very similar to an ``Assertion``, except that there is
no defined badge property. Instead, a claim property allows endorsers to make
specific claims about other ``Profiles``, ``BadgeClasses``, or ``Assertions``.

+----------------+-------------------+----------------------------------------+
| Property       | Expected type     | Description                            |
+================+===================+========================================+
| **id**         | ``Text``          | Unique IRI for the endorsement         |
|                |                   | instance. If using hosted verification,|
|                |                   | this should be the URI where the       |
|                |                   | assertion of endorsement is accessible.|
+----------------+-------------------+----------------------------------------+
| **type**       | JSON-LD type      | Endorsement type;                      |
|                |                   | Fixed: ``Endorsement``.                |
+----------------+-------------------+----------------------------------------+
| **claim**      | ``RecipientClaim``| An entity, identified by an id and     |
|                | ``EDSClaim`` or   | additional properties that the endorser|
|                | ``MinistryClaim`` | would like to claim about that entity. |
|                |                   | Three claim entities have been defined |
|                |                   | based on the attached information.     |
+----------------+-------------------+----------------------------------------+
| **issuer**     | ``Profile``       | The profile of the endorsement’s       |
|                |                   | issuer.                                |
+----------------+-------------------+----------------------------------------+
| **issuedOn**   | ``DateTime``      | Timestamp of when the endorsement      |
|                |                   | was published (``ISO8601`` compliant). |
+----------------+-------------------+----------------------------------------+
|**verification**| ``Verification    | Instructions for third parties to      |
|                | Object``          | verify this endorsement.               |
+----------------+-------------------+----------------------------------------+
| **signature**  |``SignatureObject``| An extension that allows an issuer to  |
|                |                   | issue an Open Badge on the blockchain  |
|                |                   | and provide proof of inclusion in a    |
|                |                   | blockchain transaction. This uses      |
|                |                   | `Merkle Proof 2017`_ Signature Suite.  |
+----------------+-------------------+----------------------------------------+
| signatureLines | ``Array`` of      | List of handwritten signatures that    |
|                | ``SignatureLine`` | must appear in the certificate         |
|                |                   | visualization                          |
+----------------+-------------------+----------------------------------------+


Claims
------

MinistryClaim
^^^^^^^^^^^^^

Appends the information of the assertion's registry by the Education Ministry.

+-----------------+-------------------+---------------------------------------+
| Property        | Expected type     | Description                           |
+=================+===================+=======================================+
| **type**        | ``Array``         | Composed type of ``MinistryClaim``    |
|                 |                   | and ``Extension``.                    |
+-----------------+-------------------+---------------------------------------+
| **id**          | ``IRI``           | Id of the Assertion that endorsement  |
|                 |                   | is giving extra info.                 |
+-----------------+-------------------+---------------------------------------+
| **ministry\     | ``SignatureLine`` | Handwritten signature that gave       |
| Signature**     |                   | validity to academic certificates     |
|                 |                   | analogically.                         |
+-----------------+-------------------+---------------------------------------+
| **registryCode**| ``Text``          | Alphanumeric code that represents the |
|                 |                   | unique identifier of the official     |
|                 |                   | title record of the government of     |
|                 |                   | Andorra.                              |
+-----------------+-------------------+---------------------------------------+


RecipientClaim
^^^^^^^^^^^^^^

An extension of the information about the recipient of the assertion.
Mainly uses fields from a Person_

.. _Person: https://schema.org/Person

+---------------+------------------+------------------------------------------+
| Property      | Expected type    | Description                              |
+===============+==================+==========================================+
| **type**      | ``Array``        | Composed type of ``RecipientClaim``      |
|               |                  | and ``Extension``.                       |
+---------------+------------------+------------------------------------------+
| **id**        | ``IRI``          | Id of the Assertion that endorsement is  |
|               |                  | giving extra info.                       |
+---------------+------------------+------------------------------------------+
| **givenName** | ``Text``         | The name of the recipient                |
+---------------+------------------+------------------------------------------+
| **familyName**| ``Text``         | The surnames of the recipient            |
+---------------+------------------+------------------------------------------+
| **birthplace**| ``Text``         | The place where the person was born.     |
+---------------+------------------+------------------------------------------+
| **birthdate** | ``Text``         | The date when the person was born.       |
+---------------+------------------+------------------------------------------+
|**nationality**| ``Text``         | Nationality of the person ``ISO 3166-1-  |
|               |                  | alpha2``.                                |
+---------------+------------------+------------------------------------------+
| **nationalId**| ``Text``         | The national ID person of their country, |
|               |                  | e.g. the CIF/NIF in Spain or NPI in      |
|               |                  | Andorra.                                 |
+---------------+------------------+------------------------------------------+


EDSClaim
^^^^^^^^

Adds additional information to the ``Assertion`` so it can be a valid European
Diploma Supplement (`EDS`_) .

Language codes must be compatible with ``BCP47``. Think “en” or “es-MX”.
JSON-LD allows much more expressive combinations of multiple languages in one
document. It is likely that you may be able to produce Badge Objects taking
advantage of these features that will not be understood by some or all
validators or display tools. It is recommended to keep implementations as
simple as possible and communicate with the standards group when you want to
move beyond the example techniques expressed here.

It provides additional information to that included in the official degrees
/ diplomas and/or transcript, making it more easily understood, especially by
employers or institutions outside the issuing country. (explicacio numero)

.. _EDS: https://europass.cedefop.europa.eu/documents/european-skills-passport/diploma-supplement

+------------------+---------------+------------------------------------------+
| Property         | Expected type | Description                              |
+==================+===============+==========================================+
| **type**         | ``Array``     | Composed type of ``EDSClaim``            |
|                  |               | and ``Extension``.                       |
+------------------+---------------+------------------------------------------+
| **id**           | ``IRI``       | Id of the Assertion that endorsement is  |
|                  |               | giving extra info.                       |
+------------------+---------------+------------------------------------------+
| **mainField**    | ``Text``      | 2.2 Main field(s) of study for the       |
|                  |               | qualification.                           |
+------------------+---------------+------------------------------------------+
| **awarding\      | ``Text``      | 2.3 Name (in original language) and      |
| Institution**    |               | status of awarding institution.          |
+------------------+---------------+------------------------------------------+
| **administering\ | ``Text``      | 2.4 Name (in original language) and      |
| Institution**    |               | status of institution administering      |
|                  |               | studies.                                 |
+------------------+---------------+------------------------------------------+
| **language**     | ``Text``      | 2.5 Language(s) of instruction /         |
|                  |               | examination.                             |
+------------------+---------------+------------------------------------------+
| **studiesLevel** | ``Text``      | 3.1 Level of qualification.              |
+------------------+---------------+------------------------------------------+
| **studiesLength**| ``Text``      | 3.2 Official length of programme.        |
+------------------+---------------+------------------------------------------+
| **access**       | ``Text``      | 3.3 Access requirements.                 |
+------------------+---------------+------------------------------------------+
| **mode**         | ``Text``      | 4.1 Mode of study.                       |
+------------------+---------------+------------------------------------------+
| **requirements** | ``Text``      | 4.2 Programme requirements.              |
+------------------+---------------+------------------------------------------+
| **grades**       | ``Array`` of  | 4.3 Programme details.                   |
|                  | ``EDSSubject``|                                          |
|                  | items         |                                          |
+------------------+---------------+------------------------------------------+
| **gradingScheme**| ``Text``      | 4.4 Grading scheme.                      |
+------------------+---------------+------------------------------------------+
| **qualification**| ``Text``      | 4.5 Overall classification of the        |
|                  |               | qualification.                           |
+------------------+---------------+------------------------------------------+
| **further**      | ``Text``      | 5.1 Access to further study.             |
+------------------+---------------+------------------------------------------+
| **competences**  | Array of      | 5.2 Professional status and competences. |
|                  | ``Text``      |                                          |
+------------------+---------------+------------------------------------------+
| **extraInfo**    | ``Array`` of  | 6 Additional information.                |
|                  | ``Text``      |                                          |
+------------------+---------------+------------------------------------------+
| **education\     |``ImageObject``| 8 Information on the national higher     |
| System**         |               | education system Badge.                  |
+------------------+---------------+------------------------------------------+
| **rector\        |``ImageObject``| 7 Certification of the Supplement,       |
| Signature**      |               | image of the rector signature            |
+------------------+---------------+------------------------------------------+
| **manager\       |``ImageObject``| 7 Certification of the Supplement,       |
| Signature**      |               | image of the manager signature           |
+------------------+---------------+------------------------------------------+



EDSSubject
""""""""""

This object includes all the information related to one subject that will be
included in the European Diploma Supplement (`EDS`_).

+-------------+---------------+-----------------------------------------------+
| Property    | Expected type | Description                                   |
+=============+===============+===============================================+
| **name**    | ``Text``      | Name of subject                               |
+-------------+---------------+-----------------------------------------------+
| **choice**  | ``Text``      | Type of the subject, it could be one of the   |
|             |               | following:                                    |
|             |               | ``OB``: Obligatory; ``OP``: Optional;         |
|             |               | ``LL``: Free Choice                           |
+-------------+---------------+-----------------------------------------------+
| **semester**| ``Text``      | Semester during which the studies were taken  |
+-------------+---------------+-----------------------------------------------+
| **year**    | ``Number``    | Year when the subject was taken.              |
+-------------+---------------+-----------------------------------------------+
| **mobility**| ``Text``      | International academic mobility               |
|             |               | ``M`` if was studied abroad, ``-`` if not.    |
+-------------+---------------+-----------------------------------------------+
| **grade**   | ``Text``      | Grade (``CO``: if convalidated)               |
|             |               | Otherwise, the grade obtained as a float.     |
|             |               | (ie: ``8.4``). Using a dot ``.`` as decimal   |
|             |               | separator.                                    |
+-------------+---------------+-----------------------------------------------+
| **credits** | ``Number``    | Amount of ECTS_ credits the subject took.     |
+-------------+---------------+-----------------------------------------------+

.. _ECTS: https://ec.europa.eu/education/resources-and-tools/european-credit-transfer-and-accumulation-system-ects_en
