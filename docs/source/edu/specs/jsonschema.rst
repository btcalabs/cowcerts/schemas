============
JSON Schemas
============

We provide a series of `JSON Schema`_ documents to allow implementors of the
standard ensure their documents follow the structure specified by the standard.

.. _JSON Schema: https://json-schema.org

https://gitlab.com/cowcerts/schemas/tree/master/cowcerts/

> In that folder, choose a version folder, and you'll find the schemas inside 
> the ``jsonschema`` folder

You can use our *Python* script_ to check a document against our schema.

.. _script: https://gitlab.com/cowcerts/schemas/blob/master/validator.py

Just run

.. code-block:: shell

   python -m validator.py

For more information.
