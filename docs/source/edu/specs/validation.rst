==========
Validation
==========

To validate the educational digital certificates' JSON documents, the process
described in the following document must be performed.

*****
Tools
*****

This process is automatically performed by this JavaScript library:

https://gitlab.com/cowcerts/libverifier-js

And a visual verification can be triggered using a WebComponent_:

.. _WebComponent: https://www.webcomponents.org

https://gitlab.com/cowcerts/verifier

*******
Process
*******

Blockcerts Validation
=====================

The first validation step is to perform all validation steps as specified by the
`Blockcerts validation process`__.

.. _blockcerts-validation: https://github.com/blockchain-certificates/cert-verifier-js/blob/master/docs/verification-process.md

__ blockcerts-validation_

This validation has to be performed for all documents included in the certificate,
including `the endorsements included in the signature <jsonld.html#merkleproof2019>`__ field.

Cowcerts Validation
===================

TODO
