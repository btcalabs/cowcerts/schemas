# W3id.org permaid
We want permanent identificators for our certificates' specification: the
the [JSON Schemas][jsonschema] and [JSON LD][jsonld] contexts.

That's why we've registered the ``cowcerts`` space in the 
[W3id.org permaid][w3id-permaid-gh] permanent identificators project.

## URL
https://w3id.org/cowcerts

# Tools
This directory allows to test the [Apache HTTPd][apache-httpd] `.htaccess` 
rules that will be placed in [W3id.org permaid repository][w3id-permaid-gh] so
that we can refer permanently all of our files.

To do that, we use Docker to load a sample configuration with the rules that
will be placed in the [W3id.org permaid repository][w3id-permaid-gh] so that
we can test if the URLs work prior to requesting a change via a pull request.

## Usage
Modify the [w3id.org permaid submodule][w3id-permaid-gh] with some new rules,
and build and run the image. This is simplified with the `Makefile` provided:

```sh
make run
```

You will have locally a live server serving your modified copy of the 
[w3id.org permaid][w3id-permaid-gh] repository.

http://localhost:8080

> To rebuild the image and run again:
>
> ```sh
> make rerun
> ```

# Pull requests
The list of changes to the [W3id.org permaid repository][w3id-permaid-gh] with
the URLs rules are made via pull requests. Here is a list of all the pull
requests requested to the repository:

- [Cowcerts namespace to docs](https://github.com/perma-id/w3id.org/pull/1290)
- [Cowcerts for education rules](https://github.com/perma-id/w3id.org/pull/1302)

[apache-httpd]: https://httpd.apache.org
[w3id-permaid-gh]: https://github.com/perma-id/w3id.org
[jsonschema]: https://json-schema.org
[jsonld]: https://json-ld.org
