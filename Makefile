.PHONY: test tests

default: prepare test

test:
	python -m pytest tests

tests:
	pipenv run tox

prepare:
	pip install -r requirements-dev.txt

requirements:
	pipenv run pipenv_to_requirements
